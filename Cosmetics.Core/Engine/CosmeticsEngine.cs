﻿using Cosmetics.Models;

namespace Cosmetics.Core.Engine
{
    /// <summary>
    /// Supports all available operations in the Cosmetics Shop, such as Adding, Removing, Buying, etc. products and categories.
    /// </summary>
    public class CosmeticsEngine
    {
        private const string CategoryExists = "Category with name {0} already exists!";
        private const string CategoryCreated = "Category with name {0} was created!";
        private const string CategoryDoesNotExist = "Category {0} does not exist!";
        private const string ProductDoesNotExist = "Product {0} does not exist!";
        private const string ProductAddedToCategory = "Product {0} added to category {1}!";
        private const string ProductRemovedCategory = "Product {0} removed from category {1}!";
        private const string ProductAlreadyExist = "Product with name {0} already exists!";
        private const string ProductCreated = "Product with name {0} was created!";
        private const string ProductAddedToShoppingCart = "Product {0} was added to the shopping cart!";
        private const string ProductDoesNotExistInShoppingCart = "Shopping cart does not contain product with name {0}!";
        private const string ProductRemovedFromShoppingCart = "Product {0} was removed from the shopping cart!";
        private const string TotalPriceInShoppingCart = "${0} total price currently in the shopping cart!";

        /// <summary>
        /// Contains all available products and categories in the Cosmetics Shop.
        /// </summary>
        private ApplicationData applicationData;
        /// <summary>
        /// Contains the products that the customer wants to buy.
        /// </summary>
        private ShoppingCart shoppingCart;

        public CosmeticsEngine()
        {
            this.applicationData = new ApplicationData();
            this.shoppingCart = new ShoppingCart();
        }

        /// <summary>
        /// Tries to create a new <see cref="Category"/>. If the category already exists, an appropriate message is returned.
        /// </summary>
        /// <param name="categoryName">The name of the category</param>
        /// <returns>Message describing the result of the operation.</returns>
        public string CreateCategory(string categoryName)
        {
            if (this.applicationData.CategoryExists(categoryName))
            {
                return string.Format(CategoryExists, categoryName);
            }

            this.applicationData.CreateCategory(categoryName);

            return string.Format(CategoryCreated, categoryName);
        }

        /// <summary>
        /// Tries to add a <see cref="Product"/> to a <see cref="Category"/>. 
        /// If the product does not exist or the category does not exist, an appropriate message is returned
        /// </summary>
        /// <param name="categoryNameToAdd">The name of the category</param>
        /// <param name="productToAdd">The name of the product</param>
        /// <returns>Message describing the result of the operation.</returns>
        public string AddToCategory(string categoryNameToAdd, string productToAdd)
        {
            var category = this.applicationData.FindCategory(categoryNameToAdd);
            if (category == null)
            {
                return string.Format(CategoryDoesNotExist, categoryNameToAdd);
            }

            var product = this.applicationData.FindProduct(productToAdd);
            if (product == null)
            {
                return string.Format(ProductDoesNotExist, productToAdd);
            }

            category.AddProduct(product);

            return string.Format(ProductAddedToCategory, productToAdd, categoryNameToAdd);
        }

        /// <summary>
        /// Tries to remove a <see cref="Product"/> from a <see cref="Category"/>. 
        /// If the product does not exist or the category does not exist, an appropriate message is returned
        /// </summary>
        /// <param name="categoryName">The name of the category</param>
        /// <param name="productToRemove">The name of the product</param>
        /// <returns>Message describing the result of the operation.</returns>
        public string RemoveFromCategory(string categoryName, string productToRemove)
        {
            var category = this.applicationData.FindCategory(categoryName);
            if (category == null)
            {
                return string.Format(CategoryDoesNotExist, categoryName);
            }

            var product = this.applicationData.FindProduct(productToRemove);
            if (product == null)
            {
                return string.Format(ProductDoesNotExist, productToRemove);
            }

            category.RemoveProduct(product);

            return string.Format(ProductRemovedCategory, productToRemove, categoryName);
        }

        /// <summary>
        /// Display information about the products in a <see cref="Category"/>
        /// If the category does not exist, an appropriate message is returned.
        /// </summary>
        /// <param name="categoryToShow">The name of the category</param>
        /// <returns>Message describing the result of the operation.</returns>
        public string ShowCategory(string categoryToShow)
        {
            var category = this.applicationData.FindCategory(categoryToShow);
            if (category == null)
            {
                return string.Format(CategoryDoesNotExist, categoryToShow);
            }

            return category.Print();
        }

        /// <summary>
        /// Creates a new <see cref="Product"/> and saves it to the <see cref="ApplicationData"/>
        /// If the product exists, an appropriate message is returned.
        /// </summary>
        /// <param name="name">The name of the product</param>
        /// <param name="brand">The brand of the product</param>
        /// <param name="price">The price of the product</param>
        /// <param name="gender">The gender type of the product. Must be convertible to <see cref="GenderType"/></param>
        /// <returns>Message describing the result of the operation</returns>
        public string CreateProduct(string name, string brand, decimal price, string gender)
        {
            if (this.applicationData.ProductExists(name))
            {
                return string.Format(ProductAlreadyExist, name);
            }

            this.applicationData.CreateProduct(name, brand, price, gender);

            return string.Format(ProductCreated, name);
        }

        /// <summary>
        /// Adds a <see cref="Product"/> to the <see cref="ShoppingCart"/>.
        /// If the product does not exist, an appropriate message is returned.
        /// </summary>
        /// <param name="productName">The name of the product.</param>
        /// <returns>Message describing the result of the operation</returns>
        public string AddToShoppingCart(string productName)
        {
            var product = this.applicationData.FindProduct(productName);
            if (product == null)
            {
                return string.Format(ProductDoesNotExist, productName);
            }

            this.shoppingCart.AddProduct(product);

            return string.Format(ProductAddedToShoppingCart, productName);
        }

        /// <summary>
        /// Tries to remove a <see cref="Product"/> from the <see cref="ShoppingCart"/>.
        /// If the product does not exist or the shopping cart does not contain the product, an appropriate message is returned.
        /// </summary>
        /// <param name="productName">The name of the product.</param>
        /// <returns>Message describing the result of the operation</returns>
        public string RemoveFromShoppingCart(string productName)
        {
            var product = this.applicationData.FindProduct(productName);
            if (product == null)
            {
                return string.Format(ProductDoesNotExist, productName);
            }

            if (!this.shoppingCart.ContainsProduct(product))
            {
                return string.Format(ProductDoesNotExistInShoppingCart, productName);
            }

            this.shoppingCart.RemoveProduct(product);

            return string.Format(ProductRemovedFromShoppingCart, productName);
        }

        /// <summary>
        /// Returns a message describing the total price of all products in the <see cref="ShoppingCart"/>
        /// </summary>
        /// <returns>Message about the result of the operation.</returns>
        public string CalculateTotalPrice()
        {
            decimal totalPrice = this.shoppingCart.TotalPrice();
            if (totalPrice==0)
            {
                return "No product in shopping cart!";
            }
            return string.Format(TotalPriceInShoppingCart, totalPrice);
        }
    }
}
