﻿using Cosmetics.Models;
using System;
using System.Collections.Generic;

namespace Cosmetics.Core.Engine
{
    public class ApplicationData
    {
        private List<Category> categories;
        private List<Product> products;

        public ApplicationData()
        {
            this.categories = new List<Category>();
            this.products = new List<Product>();
        }

        /// <summary>
        /// Creats a new <see cref="Category" and saves it to the collection of categories./>
        /// </summary>
        /// <param name="categoryName">The name of the category.</param>
        public void CreateCategory(string categoryName)
        {
            var category = new Category(categoryName);
            this.categories.Add(category);
        }

        /// <summary>
        /// Tries to find a <see cref="Category"/> by its <paramref name="categoryName"/>.
        /// If no such category exists, null is returned.
        /// </summary>
        /// <param name="categoryName">The name of the category</param>
        /// <returns>Category if found, null otherwise.</returns>
        public Category FindCategory(string categoryName)
        {
            foreach (var category in this.categories)
            {
                if (category.Name == categoryName)
                {
                    return category;
                }
            }

            // if we reach here, the category does not exist
            return null;
        }

        /// <summary>
        /// Checks if a <see cref="Category"/> with a given <paramref name="categoryName"/> exists in the AppData.
        /// </summary>
        /// <param name="categoryName">The name of the category</param>
        /// <returns>Bool indicating the result of the operation.</returns>
        public bool CategoryExists(string categoryName)
        {
            return this.FindCategory(categoryName) != null;
        }

        /// <summary>
        /// Creats a new <see cref="Product" and saves it to the collection of categories./>
        /// </summary>
        /// <param name="name">The name of the product</param>
        /// <param name="brand">The brand of the product</param>
        /// <param name="price">The price of the product</param>
        /// <param name="gender">The gender type of the product. Must be convertible to <see cref="GenderType"/></param>
        public void CreateProduct(string name, string brand, decimal price, string gender)
        {
            if (!Enum.TryParse(gender, out GenderType parseResult))
            {
                throw new ArgumentException($"Cannot convert {gender} to GenderType");
            }

            var product = new Product(name, brand, price, parseResult);

            this.products.Add(product);
        }

        /// <summary>
        /// Tries to find a <see cref="Product"/> by its <paramref name="productName"/>.
        /// If no such category exists, null is returned.
        /// </summary>
        /// <param name="productName">The name of the product</param>
        /// <returns>Product if found, null otherwise.</returns>
        public Product FindProduct(string productName)
        {
            foreach (var product in this.products)
            {
                if (product.Name == productName)
                {
                    return product;
                }
            }

            // if we reach here, the product does not exist
            return null;
        }

        /// <summary>
        /// Checks if a <see cref="Product"/> with a given <paramref name="productName"/> exists in the AppData.
        /// </summary>
        /// <param name="productName">The name of the product.</param>
        /// <returns>Bool indicating the result of the operation.</returns>
        public bool ProductExists(string productName)
        {
            return this.FindProduct(productName) != null;
        }
    }
}
