﻿using System;
using System.Text;

namespace Cosmetics.Models
{
    public class Product
    {
        //Fields
        private string name;
        private string brand;
        private decimal price;
        private GenderType gender;
        //Constructor
        public Product(string name, string brand, decimal price, GenderType gender)
        {
            this.Name = name;
            this.Brand = brand;
            this.Price = price;
            this.Gender = gender;
        }
        //Properties
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value.Length < 3 || value.Length > 10)
                {
                    throw new ArgumentOutOfRangeException("Please add a name within the range of 3 and 10 symbols");
                }
                else if (value == null)
                {
                    throw new ArgumentNullException("Brand name cannot be empty.");
                }
                this.name = value;
            }
        }

        public string Brand
        {
            get
            {
                return this.brand;
            }
            set
            {
                if (value.Length<2 || value.Length>10)
                {
                    throw new ArgumentOutOfRangeException("Please add a brand name within the range of 2 and 10 letters.");
                }
                else if (value == null)
                {
                    throw new ArgumentNullException("Brand name cannot be empty.");
                }
                this.brand = value;
            }
        }

        public decimal Price
        {
            get
            {
                return this.price;
            }
            set
            {
                if (value<0)
                {
                    throw new ArgumentOutOfRangeException("Please add a price which is positive number.");
                }
                this.price = value;
            }
        }

        public GenderType Gender
        {
            get
            {
                return this.gender;
            }
            private set
            {
                this.gender = value;
            }
        }
        //Methods
        public string Print()
        {
            StringBuilder output = new StringBuilder();
            output.AppendLine($" #{this.Name} {this.Brand}");
            output.AppendLine($" #Price: ${this.Price}");
            output.AppendLine($" #Gender: {this.Gender}");
            output.AppendLine(" ===");
            return output.ToString().TrimEnd();
        }
    }
}
