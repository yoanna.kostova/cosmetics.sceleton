﻿namespace Cosmetics.Models
{
    public enum GenderType
    {
        Men = 0,
        Women = 1,
        Unisex = 2
    }
}
