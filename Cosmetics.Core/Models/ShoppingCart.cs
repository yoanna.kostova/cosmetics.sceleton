﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmetics.Models
{
    public class ShoppingCart
    {
        //Constructor
        private List<Product> productList;
        
        public ShoppingCart()
        {
            productList = new List<Product>();
            
        }
        //Methods

        public void AddProduct(Product product)
        {
            productList.Add(product);
        }

        public void RemoveProduct(Product product)
        {
            productList.Remove(product);
        }

        public bool ContainsProduct(Product product)
        {
            return productList.Contains(product);
        }

        public decimal TotalPrice()
        {
            decimal sum = 0;
            for (int i = 0; i < productList.Count; i++)
            {
                sum += productList[i].Price;
            }
            return sum;
        }
    }
}
