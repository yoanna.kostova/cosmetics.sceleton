﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cosmetics.Models
{
    public class Category
    {
        //Fields
        private string name;
        private List<Product> products;
        
        //Constructor
        public Category(string name)
        {
            this.Name = name;
            products = new List<Product>();
        }
        //Properties

        //DONE
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 15)
                {
                    throw new ArgumentOutOfRangeException("Please provide a name within the range os 2 and 15 symbols.");
                }
                this.name = value;
            }
        }
        public List<Product> Products
        {
            get { return this.products; }
        }

        //Methods
        public void AddProduct(Product product)
        {
            products.Add(product);
        }

        public void RemoveProduct(Product product)
        {
            products.Remove(product);
        }
        public string Print()
            
        {
            StringBuilder currentProduct = new StringBuilder();
            currentProduct.AppendLine($"#Category: {this.name}");
            if (products.Count == 0)
            {
                currentProduct.AppendLine($" #No products in this category");
            }

            List<Product> productsSorted = products.OrderBy(p => p.Brand).ThenByDescending(p => p.Price).ToList();

            foreach (var item in productsSorted)
            {
                currentProduct.AppendLine(item.Print());
            }

            return currentProduct.ToString().TrimEnd();
        }
    }
}

