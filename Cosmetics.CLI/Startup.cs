﻿using Cosmetics.Core.Engine;
using System;

namespace Cosmetics
{
    public class Startup
    {
        public static void Main()
        {
            var engine = new CosmeticsEngine();

            Console.WriteLine(engine.CreateProduct("MyMan", "Nivea", 10.99m, "Men"));
            Console.WriteLine(engine.CreateCategory("Shampoos"));
            Console.WriteLine(engine.AddToCategory("Shampoos", "MyMan"));
            Console.WriteLine(engine.AddToShoppingCart("MyMan"));
            Console.WriteLine(engine.ShowCategory("Shampoos"));
            Console.WriteLine(engine.CalculateTotalPrice());
            Console.WriteLine(engine.RemoveFromCategory("Shampoos", "MyMan"));
            Console.WriteLine(engine.ShowCategory("Shampoos"));
            Console.WriteLine(engine.RemoveFromShoppingCart("MyMan"));
            Console.WriteLine(engine.CalculateTotalPrice());
        }
    }
}
